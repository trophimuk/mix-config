let mix = require('laravel-mix');

let ImageminPlugin = require( 'imagemin-webpack-plugin' ).default;
mix.webpackConfig( {
    plugins: [
        new ImageminPlugin( {
            // disable: process.env.NODE_ENV !== 'production', // Disable during development
            pngquant: {
                quality: '95-100',
            },
            test: /\.(jpe?g|png|gif|svg)$/i,
        } ),
    ],
} ) 

mix.js('src/js/main.js', 'dist')
   .sass('src/scss/main.scss', 'dist')
   .copy( 'src/img', 'dist/img', false )
   .copy( 'src/font', 'dist/font', false )
   .options({ processCssUrls: false });